# SpringBoot整合微信支付

## 256位秘钥策略与JDK8的小Bug

如果使用JDK8运行后端时遇到报错解决:java.security.InvalidKeyException: Illegal key size(微信支付v3遇到的问题)

原因是因为微信支付256位秘钥策略可能会导致某些jdk的版本加密解密出现问题，首先观察你这个目录下的文件

![image-20230317231340561](assets/image-20230317231340561.png)

根据文件内容做判断看下目录里面是有一个 policy 文件夹，还是有local_policy.jar

去官方下载JCE无限制权限策略文件，这里贴出jdk 8的国内地址 方便下载https://wwi.lanzoup.com/iXGs404zm1dg

![image-20230317231652843](assets/image-20230317231652843.png)

下载解压后，将以上两个文件拷贝覆盖到Java\jdk1.8.0_152\jre\lib\security目录中

## 请参阅

了解更多请参考CSDN博客：https://blog.csdn.net/yueyue763184/article/details/129624617?spm=1001.2014.3001.5501
