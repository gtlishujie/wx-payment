package com.bls.payment.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bls.payment.entity.Product;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ProductMapper extends BaseMapper<Product> {
}
