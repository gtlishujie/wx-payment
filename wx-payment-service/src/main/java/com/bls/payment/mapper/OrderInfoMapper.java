package com.bls.payment.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bls.payment.entity.OrderInfo;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OrderInfoMapper extends BaseMapper<OrderInfo> {

}
