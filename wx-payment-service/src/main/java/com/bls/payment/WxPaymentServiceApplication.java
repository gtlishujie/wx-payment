package com.bls.payment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WxPaymentServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(WxPaymentServiceApplication.class, args);
    }

}
