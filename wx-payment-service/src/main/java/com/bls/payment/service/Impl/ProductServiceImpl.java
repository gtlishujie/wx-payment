package com.bls.payment.service.Impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bls.payment.entity.Product;
import com.bls.payment.mapper.ProductMapper;
import com.bls.payment.service.ProductService;
import org.springframework.stereotype.Service;

@Service
public class ProductServiceImpl extends ServiceImpl<ProductMapper, Product> implements ProductService {
}
